<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WorkshopsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 * @return void
	 * @throws Exception
	 */
	public function run()
	{
		$userIds = User::all()->pluck('id')->toArray();
		$userIds = array_values($userIds);

		for($i = 1; $i <= 10; $i++) {
			$from = new Carbon('first day of this month');
			$from->addDays(random_int(0, 45));
			$from->setTime(0, 0);
			$days = random_int(1, 4);
			$to = $from->copy()->addDays($days);

			DB::table('workshops')->insert([
				'user_id' => $userIds[array_rand($userIds)],
				'name' => "Workshop #{$i}",
				'trainer_id' => null,
				'description' => Str::random(100),
				'cancellation_policy' => Str::random(100),
				'from' => $from,
				'to' => $to,
				'price_per_day' => 50,
				'price_total' => 50 * $days,
				'created_at' => new Carbon(),
			]);
		}

	}
}
