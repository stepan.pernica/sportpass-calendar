<?php

use App\User;
use App\Workshop;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WorkshopsOrdersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 * @return void
	 * @throws Exception
	 */
	public function run()
	{
		$userIds = User::all()->pluck('id')->toArray();
		$userIds = array_values($userIds);

		$workshopIds = Workshop::all()->pluck('id')->toArray();
		$workshopIds = array_values($workshopIds);

		for($i = 1; $i <= 10; $i++) {
			$workshopId = $workshopIds[array_rand($workshopIds)];
			$partReservation = false;
			$partReservationDate = null;
			if ($i % 2 === 0) {
				$workshop = Workshop::find($workshopId);
				$partReservation = true;
				$partReservationDate = $workshop->from;
			}

			DB::table('workshops_orders')->insert([
				'user_id' => $userIds[array_rand($userIds)],
				'workshop_id' => $workshopId,
				'part_reservation' => $partReservation,
				'part_reservation_date' => $partReservationDate,
				'paid' => random_int(0, 1),
				'created_at' => new Carbon(),
			]);
		}

	}
}
