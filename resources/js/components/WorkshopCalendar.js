import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";

export default class WorkshopCalendar extends Component {
    constructor (props) {
        super(props)
    }

    render() {
        return (
            <FullCalendar
                defaultView="dayGridMonth"
                weekends={true}
                locale="en"
                firstDay={1}
                plugins={[ dayGridPlugin, interactionPlugin ]}
                selectable={true}
                select={this.handleDateSelect}
                timeFormat="H(:mm)"
                displayEventTime={false}
                events={{
                    url: '/api/workshopsPerMonth',
                    method: 'GET'
                }}
            />
        )
    };

    handleDateSelect = (arg) => {
        const { history } = this.props;
        history.push('/create?from=' + arg.startStr + '&to=' + arg.endStr);
    };
}
