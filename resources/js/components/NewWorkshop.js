import axios from 'axios'
import React, { Component } from 'react'
import {Link} from "react-router-dom";
import queryString from 'query-string'

class NewWorkshop extends Component {
  constructor (props) {
    super(props)

    this.state = {
      id: null,
      from: '',
      to: '',
      picture: null,
      name: '',
      trainer_id: '',
      description: '',
      price_per_day: '',
      price_total: '',
      cancellation_policy: '',
      repeat: false,
      repeat_type: 'weekly',
      repeat_count: 1,
      errors: []
    }

    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handleCreateNewWorkshop = this.handleCreateNewWorkshop.bind(this)
    this.hasErrorFor = this.hasErrorFor.bind(this)
    this.renderErrorFor = this.renderErrorFor.bind(this)
  }

  componentDidMount () {
    const values = queryString.parse(this.props.location.search);
    //const projectId = this.props.match.params.id;
    const projectId = values.id;

    //console.log(projectId);

    if (projectId) {
      axios.get(`/api/workshops/${projectId}`).then(response => {
        this.setState({
          id: response.data.id,
          from: response.data.from,
          to: response.data.to,
          name: response.data.name,
          //picture: response.data.picture,
          //trainer_id: response.data.trainer_id,
          description: response.data.description,
          price_per_day: response.data.price_per_day,
          price_total: response.data.price_total,
          cancellation_policy: response.data.cancellation_policy
        })
      })
    } else {
      const values = queryString.parse(this.props.location.search);
      this.setState({
        from: values.from,
        to: values.to,
      })
    }
  }

  handleFieldChange (event) {
    if (event.target.name === 'repeat') {
      if (event.target.checked) {
        $('#repeatParentDiv').removeClass('collapse');
      } else {
        $('#repeatParentDiv').addClass('collapse');
      }

      this.setState({
        [event.target.name]: event.target.checked ? true : false
      })
    } else {
      this.setState({
        [event.target.name]: event.target.value
      })
    }
  }

  handleImageChange = event => {
    this.setState({
      picture: event.target.files[0]
    });
  }

  handleCreateNewWorkshop (event) {
    event.preventDefault();
    const { history } = this.props;

    var workshop = this.state;
    // todo image
    //console.log(workshop.picture);
    //const formData = new FormData();
    //formData.append('picture', workshop.picture)

    axios.post(
          '/api/workshops',
          workshop,
          /*{
          headers: {
            'content-type': 'multipart/form-data'
          }
        }*/)
      .then(response => {
        history.push('/')
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        })
      })
  }

  hasErrorFor (field) {
    return !!this.state.errors[field]
  }

  renderErrorFor (field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className='invalid-feedback'>
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      )
    }
  }

  render () {
    return (
      <div className='container py-4'>
        {this.state.id &&
            <div className="text-right mb-2">
              <a href={'/workshop/delete/' + this.state.id} className="btn btn-danger">Delete this workshop (not implemented)</a>
            </div>
        }

        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <div className='card'>
              <div className='card-header'>
                {this.state.id ? 'Edit workshop' : 'Create workshop'}
              </div>

              <div className='card-body'>

                <form onSubmit={this.handleCreateNewWorkshop} id='workshop-form'>

                  <div className='form-group row'>
                    <label htmlFor='from' className='col-sm-3 col-form-label'>From</label>
                    <div className="col-sm-9">
                      <input
                          id='from'
                          type='text'
                          className={`form-control ${this.hasErrorFor('from') ? 'is-invalid' : ''}`}
                          name='from'
                          value={this.state.from}
                          onChange={this.handleFieldChange}
                      />
                      {this.renderErrorFor('from')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='to' className='col-sm-3 col-form-label'>To</label>
                    <div className="col-sm-9">
                      <input
                          id='to'
                          type='text'
                          className={`form-control ${this.hasErrorFor('to') ? 'is-invalid' : ''}`}
                          name='to'
                          value={this.state.to}
                          onChange={this.handleFieldChange}
                      />
                      {this.renderErrorFor('to')}
                    </div>
                  </div>

                  {!this.state.id &&
                      <div>
                        <div className='form-group row'>
                          <label htmlFor='repeat' className='col-sm-3 col-form-label'>Repeat event</label>
                          <div className="col-sm-9">
                            <input
                                id='repeat'
                                type='checkbox'
                                className={`${this.hasErrorFor('repeat') ? 'is-invalid' : ''}`}
                                name='repeat'
                                value={this.state.repeat}
                                onChange={this.handleFieldChange}
                            />
                            {this.renderErrorFor('repeat')}
                          </div>
                        </div>

                        <div id="repeatParentDiv" className="collapse">
                          <div className='form-group row'>
                            <label htmlFor='repeat_type' className='col-sm-3 col-form-label'>Repeat type</label>
                            <div className="col-sm-9">
                              <select id='repeat_type'
                                    className={`form-control ${this.hasErrorFor('repeat_type') ? 'is-invalid' : ''}`}
                                    name='repeat_type'
                                    value={this.state.repeat_type}
                                    onChange={this.handleFieldChange}>
                                <option value="weekly">weekly</option>
                                <option value="monthly">monthly</option>
                              </select>
                              {this.renderErrorFor('repeat_type')}
                            </div>
                          </div>

                          <div className='form-group row'>
                            <label htmlFor='repeat_count' className='col-sm-3 col-form-label'>How many events you want?</label>
                            <div className="col-sm-9">
                              <input
                                  id='repeat_count'
                                  type='number'
                                  className={`form-control ${this.hasErrorFor('repeat_count') ? 'is-invalid' : ''}`}
                                  name='repeat_count'
                                  value={this.state.repeat_count}
                                  onChange={this.handleFieldChange}
                              />
                              {this.renderErrorFor('repeat_count')}
                            </div>
                          </div>
                        </div>
                      </div>
                  }

                  <div className='form-group row'>
                    <label htmlFor='picture' className='col-sm-3 col-form-label'>Picture</label>
                    <div className="col-sm-9">
                      <input
                        id='picture'
                        type='file'
                        className={`form-control ${this.hasErrorFor('picture') ? 'is-invalid' : ''}`}
                        name='picture'
                        accept='image/*'

                        onChange={this.handleImageChange}
                      />
                      {this.renderErrorFor('picture')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='name' className='col-sm-3 col-form-label'>Name</label>
                    <div className="col-sm-9">
                        <input
                          id='name'
                          type='text'
                          className={`form-control ${this.hasErrorFor('name') ? 'is-invalid' : ''}`}
                          name='name'
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor('name')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='trainer_id' className='col-sm-3 col-form-label'>Trainer</label>
                    <div className="col-sm-9">
                      <select
                          id='trainer_id'
                          className={`form-control ${this.hasErrorFor('trainer_id') ? 'is-invalid' : ''}`}
                          name='trainer_id'
                          value={this.state.trainer_id}
                          onChange={this.handleFieldChange}
                      >
                        <option value="">-- not set --</option>
                      </select>
                      {this.renderErrorFor('trainer_id')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='description' className='col-sm-3 col-form-label'>Description</label>
                    <div className="col-sm-9">
                      <textarea
                        id='description'
                        className={`form-control ${this.hasErrorFor('description') ? 'is-invalid' : ''}`}
                        name='description'
                        rows='4'
                        value={this.state.description}
                        onChange={this.handleFieldChange}
                      />

                      {this.renderErrorFor('description')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='price_per_day' className='col-sm-3 col-form-label'>Price per day</label>
                    <div className="col-sm-9">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">€</span>
                        </div>
                        <input
                            id='price_per_day'
                            type='text'
                            className={`form-control ${this.hasErrorFor('price_per_day') ? 'is-invalid' : ''}`}
                            name='price_per_day'
                            value={this.state.price_per_day}
                            onChange={this.handleFieldChange}
                        />
                      </div>
                      {this.renderErrorFor('name')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='price_total' className='col-sm-3 col-form-label'>Price for whole</label>
                    <div className="col-sm-9">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">€</span>
                        </div>
                        <input
                            id='price_total'
                            type='text'
                            className={`form-control ${this.hasErrorFor('price_total') ? 'is-invalid' : ''}`}
                            name='price_total'
                            value={this.state.price_total}
                            onChange={this.handleFieldChange}
                        />
                      </div>
                      {this.renderErrorFor('price_total')}
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label htmlFor='cancellation_policy' className='col-sm-3 col-form-label'>Cancellation policy</label>
                    <div className="col-sm-9">
                      <textarea
                          id='cancellation_policy'
                          className={`form-control ${this.hasErrorFor('cancellation_policy') ? 'is-invalid' : ''}`}
                          name='cancellation_policy'
                          rows='4'
                          value={this.state.cancellation_policy}
                          onChange={this.handleFieldChange}
                      />

                      {this.renderErrorFor('cancellation_policy')}
                    </div>
                  </div>

                  <div className="row">
                    <div className="offset-md-3 col-md-9">
                      <button className='btn btn-primary'>{this.state.id ? 'Edit' : 'Create'}</button>
                      <Link className='btn btn-secondary' to='/'>
                        Cancel
                      </Link>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default NewWorkshop
