<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkshopOrder
 * @package App
 * @mixin Builder
 */
class WorkshopOrder extends Model
{
	protected $table = 'workshops_orders';

	protected $dates = [
		'part_reservation_date',
		'created_at',
		'updated_at',
	];
}
