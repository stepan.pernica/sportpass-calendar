<?php

namespace App\Http\Controllers;

use App\Workshop;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WorkshopController extends Controller
{
	public function index(): void
	{

	}



	/**
	 * @param Request $request
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function getMonthWorkshops(Request $request): JsonResponse
	{
		$start = $request->input('start');
		$end = $request->input('end');
		return response()->json(Workshop::getWorkshopsForCalendar($start, $end));
	}



	/**
	 * @param Request $request
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function getMonthWorkshopsUser(Request $request): JsonResponse
	{
		$start = $request->input('start');
		$end = $request->input('end');
		return response()->json(Workshop::getWorkshopsForCalendarUser($start, $end));
	}



	/**
	 * @param $id
	 * @return string
	 */
	public function show($id): string
	{
		$project = Workshop::find($id);
		return $project->toJson();
	}



	/**
	 * @param Request $request
	 */
	public function store(Request $request): void
	{
		$data = $request->validate([
			'id' => 'nullable|numeric',
			'from' => 'date',
			'to' => 'date',
			'name' => 'required|max:255',
			'trainer_id' => 'nullable|int',
			'description' => 'nullable|string',
			'price_per_day' => 'nullable|numeric',
			'price_total' => 'nullable|numeric',
			'cancellation_policy' => 'nullable|string',
			'picture' => 'nullable|image|mimes:jpeg,png,jpg,gif',
			'repeat' => 'nullable|boolean',
			'repeat_type' => 'nullable|string',
			'repeat_count' => 'nullable|numeric|min:1',
		]);

		$id = (int) $data['id'];

		$dataToSave = [
			'from' => $data['from'],
			'to' => $data['to'],
			'name' => $data['name'],
			'trainer_id' => $data['trainer_id'],
			'description' => $data['description'],
			'price_per_day' => $data['price_per_day'],
			'price_total' => $data['price_total'],
			'cancellation_policy' => $data['cancellation_policy'],
		];

		if ($data['repeat'] !== true) {
			$workshop = $id ? Workshop::find($id) : new Workshop;
			$workshop->fill($dataToSave);
			$workshop->save();
		} else {

			$copies = $data['repeat_count'];
			$repeatWeekly = $data['repeat_type'] === 'weekly';

			$items = [];
			$from = new Carbon($dataToSave['from']);
			$to = new Carbon($dataToSave['to']);
			$days = $from->diff($to)->days;

			for($i = 0; $i < $copies; $i++) {
				$from = $from->copy();
				$to = $to->copy();

				$item = $dataToSave;
				if ($i !== 0) {
					if ($repeatWeekly) {
						$from->addWeek();
					} else {
						$from->addMonth();
					}

					$to = $from->copy()->addDays($days);

					$item['from'] = $from;
					$item['to'] = $to;
				}
				$items[] = $item;
			}

			Workshop::insert($items);
		}

		// todo save picture
		/*if($request->hasFile('picture')) {
			$picture = $request->file('picture');
			$picture->move(public_path("/workshops/{$workshop->id}"), $picture->getFilename());
			$workshop->picture = $picture->getFilename();
			$workshop->save();
		}*/
	}
}
